## Cheamo Entrance Tracker

This is an entrance and item randomizer tracker for [Link to the Past Randomizer](https://alttpr.com/en).
Important entrances or visibile items can be marked on the map as the player finds them.
Alternatively the tracker has premarked entrance or item locations that can be cleared off as the player finds them.
There tracker also has the ability to parse entrance spoiler logs and mark entrances on the map.

### Preview

![Light World Preview](src/images/preview/LightWorldPreview.PNG) ![Dark World Preview](src/images/preview/DarkWorldPreview.PNG)

### Background
The original intention of the project was to mimic the functionality of Seph's Tracker after the creater left the community and did not leave any source or anything that could be maintained or added to. The goal was to make it web based so it could be cross platform and make use of regular browser features and add ons. As the tracker came along it became fairly simple to expand to use for more than just entrance tracking, so presets for both entrance and item tracking were added.

### Running

The app is deployed to and can be used at [http://cheamo-er-tracker.surge.sh/](http://cheamo-er-tracker.surge.sh/)

To use locally, set up with `npm install` and run with `npm start`. 
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

## Usage

### Placing and using icons
Click anywhere on the map and it will bring up a menu of icons. 
Select one and it will place it on the map where you first clicked.
The icon will be selected (indicated by all arrows being present). 
You can then set a direction by clicking the arrows or using keyboard arrows.
Icons can be dragged around the map, and removed by right clicking on them.

### Presets
The tracker comes with presets for both entrance and item tracking, and can also be cleared of all icons.
This has every spot to check in the game (for regular modes) pre marked so that they can be cleared or set to something as the player checks them.
To open a preset click the menu button in the top right and then the reset button.
This opens a menu to reset all icons to one of the three presets.

### Save Files
The tracker keeps all icons tracked in local browser storage so the page can be refreshed or left without issue, everything will remain there.
In addition it can save and load files if the player wants to come back to a tracked game at a later time, or to create a custom preset.
To save and load click the menu button in the top right and then the appropiate save/load button and a prompt for a file will open.

### Spoiler Files
For entrance randomizer modes where the player has the spoiler ahead of time.
The tracker is able to parse the spoiler log and mark most important entrances.
To use this functionality click the menu button and then the open file button, and select the spoiler log.
An example spoiler log can be downloaded [here](src/images/preview/entrance_spoiler.txt)

## Extra Info

### Deployment with Surge
This is how the app is deployed to it's free hosting on Surge
`npm run build`
`surge` -> point project at /build and enter domain

#### Credits
[Seph's Tracker](https://cdn.discordapp.com/attachments/595019641147883539/613447032476008463/Sephs_Alttp_Entrance_Mapper.exe) <br />
[ALttPR](https://alttpr.com/en) <br />
[Surge](https://surge.sh/) <br />