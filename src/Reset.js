import React, { Component } from 'react'
import { Layer, Rect, Group, Text } from 'react-konva'
import MyImage from './MyImage'
import redX from './images/temp.png'
import door from './images/door.png'
import chest from './images/misc/chest.png'

const CONFIRM_WIDTH = 200
const CONFIRM_HEIGHT = 100
const CONFIRM_ICON_SIZE = 32

class Reset extends Component {
	render() {
		const {mapSize, onCancel, onConfirm, onPremarkedEntrances, onPremarkedItems} = this.props
		return <Layer>
			<Rect width={mapSize} height={mapSize} onClick={onCancel} />
			<Group x={mapSize / 2 - CONFIRM_WIDTH / 2} y={mapSize / 2 - CONFIRM_HEIGHT}>
				<Rect 
					fill='black'
					opacity={0.8}
					width={CONFIRM_WIDTH}
					height={CONFIRM_HEIGHT}
				/>
				<Text
					text="Reset all icons?"
					fill='white'
					align="center"
					width={CONFIRM_WIDTH}
					y={CONFIRM_HEIGHT / 8}
				/>
				<Text
					text="Entrance"
					fill='white'
					width={CONFIRM_WIDTH / 2}
					align="center"
					y={CONFIRM_HEIGHT * 1 / 3}
				/>
				<Text
					text="Item"
					fill='white'
					align="center"
					width={CONFIRM_WIDTH}
					y={CONFIRM_HEIGHT * 1 / 3}
				/>
				<Text
					text="Clear"
					fill='white'
					align="center"
					width={CONFIRM_WIDTH / 2}
					x={CONFIRM_WIDTH / 2}
					y={CONFIRM_HEIGHT * 1 / 3}
				/>
				<MyImage
					src={door}
					x={CONFIRM_WIDTH / 4 - CONFIRM_ICON_SIZE / 2}
					y={CONFIRM_HEIGHT / 2}
					width={CONFIRM_ICON_SIZE}
					height={CONFIRM_ICON_SIZE}
					onClick={onPremarkedEntrances}
					onTap={onPremarkedEntrances}/>
				<MyImage
					src={chest}
					x={CONFIRM_WIDTH * 2 / 4 - CONFIRM_ICON_SIZE / 2}
					y={CONFIRM_HEIGHT / 2}
					width={CONFIRM_ICON_SIZE}
					height={CONFIRM_ICON_SIZE}
					onClick={onPremarkedItems}
					onTap={onPremarkedItems}/>
				<MyImage
					x={CONFIRM_WIDTH * 3 / 4  - CONFIRM_ICON_SIZE / 2}
					y={CONFIRM_HEIGHT / 2}
					src={redX}
					width={CONFIRM_ICON_SIZE}
					height={CONFIRM_ICON_SIZE}
					onClick={onConfirm}
					onTap={onConfirm}/>
			</Group>
		</Layer>
	}
}

export default Reset