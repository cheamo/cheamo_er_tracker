import React, { Component } from 'react'
import {Text, Group} from 'react-konva'




class ItemSelectMenu extends Component {
	render() { 
		const {width, x, y, isEdit, onSelectToggle} = this.props
		return <Group x={x} y={y}>
			<Text
				text='Select'
				fill='white'
				textDecoration={isEdit ? 'none' : 'underline'}
				fontSize={14}
				align='left'
				fontStyle={isEdit ? 'none' : 'bold'}
				width={width / 2}
				onClick={onSelectToggle}
				onTap={onSelectToggle}
			/>
			<Text
				text='Edit'
				x={width / 2-15}
				fill='white'
				textDecoration={isEdit ? 'underline' : 'none'}
				fontSize={14}
				align='left'
				fontStyle={isEdit ? 'bold' : 'none'}
				width={width / 2}
				onClick={onSelectToggle}
				onTap={onSelectToggle}
			/>
		</Group>
	}
}

export default ItemSelectMenu