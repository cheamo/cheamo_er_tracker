import React, {Component} from 'react'
import {Rect, Group} from 'react-konva'
import ItemSelectMenu from './ItemSelectMenu'
import Icon from './Icon'

const ICON_TILE_SIZE = 48
const ICON_SIZE = 32
const MARGIN_SIZE = 10

const getX = (index, extraSpacing, numColumns) => {
	const columnInRow = index % numColumns
	return columnInRow * ICON_TILE_SIZE + (ICON_TILE_SIZE - ICON_SIZE) / 2 + columnInRow * extraSpacing
}

const getY = (index, numColumns) => {
	return Math.floor(index / numColumns) * ICON_TILE_SIZE + (ICON_TILE_SIZE - ICON_SIZE) / 2
}

class ItemSelector extends Component {
	render() {
		const {listOfItems, listOfImages, maxWidth, isEdit} = this.props
		const disabledImages = this.props.disabledImages || {}
		const filteredItemKeys = isEdit ? Object.keys(listOfItems) : Object.keys(listOfItems).filter(key => !(disabledImages[key]))
		const numRows = Math.ceil(Object.keys(listOfImages).length * ICON_TILE_SIZE / (this.props.maxWidth))
		const numColumns = Math.floor(maxWidth / ICON_TILE_SIZE)
		const extraSpacing = Math.floor((maxWidth - numColumns * ICON_TILE_SIZE) / numColumns)
		const onClick = isEdit ? this.props.onIconToggle : this.props.onSelect
		return <Group>
			<Rect
				opacity={0.6}
				fill='black'
				width={this.props.maxWidth}
				height={numRows * ICON_TILE_SIZE + MARGIN_SIZE * 2}
			/>
			<Group y={15}>
				{filteredItemKeys.map((key, index) => {
					const opacity = isEdit && disabledImages[key] ? 0.5 : 1
					return <Icon 
						key={index}
						srcName={key}
						src={listOfImages[key]}
						text={listOfItems[key].text} 
						x={getX(index, extraSpacing, numColumns)} 
						y={getY(index, numColumns)}
						width={ICON_SIZE}
						height={ICON_SIZE}
						opacity={opacity}
						alt="info"
						handleIconClick={onClick} 
						draggable={false}/>
					}
				)}
			</Group>
			<ItemSelectMenu width={140} x={10} y={5} isEdit={isEdit} onSelectToggle={this.props.onSelectToggle}/>
		</Group>
	}
}

export default ItemSelector