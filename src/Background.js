import React, { Component } from 'react'
import { Layer } from 'react-konva'
import MyImage from './MyImage'

class Background extends Component {
	render() {
		const {mapSize, menuSize} = this.props
		return <Layer>
			<MyImage
				src={this.props.backgroundSrc}
				width={mapSize}
				height={mapSize}
				onClick={this.props.onBackgroundClick}
				onTap={this.props.onBackgroundClick}
				preventDefault={false}
			/>
			<MyImage
				src={this.props.switchSrc}
				width={menuSize}
				height={menuSize}
				x={mapSize - menuSize}
				y={mapSize - menuSize}
				onClick={this.props.onBackgroundSwitch}
				onTouch={this.props.onBackgroundSwitch}
			/>
		</Layer>
	}
}

export default Background