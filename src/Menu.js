import React, { Component } from 'react'
import { Layer, Rect, Group } from 'react-konva'
import MyImage from './MyImage'
import menuIcon from './images/menu.png'
import loadIcon from './images/load.png'
import saveIcon from './images/save.png'
import resetIcon from './images/reset.png'

const MENU_SIZE = 32

class Menu extends Component {
	render() {
		const {rightLocation, isOpen} = this.props
		const menuScale = isOpen ? 4 : 1
		return <Layer>
			<Rect fill='black'
				x={rightLocation - menuScale * MENU_SIZE}
				width={menuScale * MENU_SIZE}
				height={MENU_SIZE}
				opacity={0.8}/>
			<MyImage
				src={menuIcon}
				width={MENU_SIZE}
				height={MENU_SIZE}
				x={rightLocation - MENU_SIZE}
				onClick={this.props.onMenu}
				onTap={this.props.onMenu}
			/>
			{isOpen &&
                <Group>
                	<MyImage
                		src={loadIcon}
                		width={MENU_SIZE}
                		height={MENU_SIZE}
                		x={rightLocation - 4 * MENU_SIZE}
                		onClick={this.props.onLoad}
                		onTap={this.props.onLoad}
                	/>
                	<MyImage
                		src={saveIcon}
                		width={MENU_SIZE}
                		height={MENU_SIZE}
                		x={rightLocation - 3 * MENU_SIZE}
                		onClick={this.props.onSave}
                		onTap={this.props.onSave}
                	/>
                	<MyImage
                		src={resetIcon}
                		width={MENU_SIZE}
                		height={MENU_SIZE}
                		x={rightLocation - 2 * MENU_SIZE}
                		onClick={this.props.onReset}
                		onTap={this.props.onReset}
                	/>
                </Group>}
		</Layer>
	}
}

export default Menu