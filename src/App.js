import React, { Component } from 'react'
import { Stage, Layer } from 'react-konva'
import uuid from 'uuid'
import ItemSelector from './ItemSelector'
import { UP, DOWN, LEFT, RIGHT } from './Icon'
import Menu from './Menu'
import Background from './Background'
import IconView from './IconView'
import premarkedEntrances from './premarked.json'
import premarkedItems from './premarked_item.json'
import lightWorld from './images/light_world.png'
import darkWorld from './images/dark_world.png'
import portal from './images/warp.png'
import redX from './images/temp.png'
import Reset from './Reset'
import imageNames from './items.json'
import SpoilerParser from './SpoilerParser'

var listOfImages = {}
var listOfItems = {}
const MAP_SIZE = 600
const ICON_SIZE = 20
const MENU_SIZE = 32
const ICONS = 'icons'
const DISABLED= 'disabled'

const addToArrayIfExists = (array, element) => {
	return element 
		? [...array, element]
		: array
}

const getStateForDirection = (prevState, direction) => {
	return {
		...prevState,
		selectedIcon: null,
		icons: [...prevState.icons, {...prevState.selectedIcon, direction: direction}]
	}
}

const getStateForSelectedDeleteIcon = (prevState) => {
	return {
		...prevState,
		selectedIcon: null
	}
}

const moveIconIfMatches = (icon, e) => {
	return icon !== null && icon.id === e.target.attrs.id 
		? {...icon, x: e.target.attrs.x, y: e.target.attrs.y}
		: icon
}

const validateLoadedIcons = icons => {
	return Array.isArray(icons) &&
    icons.filter(icon => {
    	return icon.hasOwnProperty('id') &&
        icon.hasOwnProperty('x') &&
        icon.hasOwnProperty('y') &&
        icon.hasOwnProperty('src') &&
        icon.hasOwnProperty('direction') &&
        icon.hasOwnProperty('lightWorld')
    }).length > 0
}

const validateLoadedSpoiler = spoiler => {
	return typeof spoiler === 'object' && spoiler !== null &&
        spoiler.hasOwnProperty('Entrances') &&
		spoiler.Entrances.length > 0 &&
		spoiler.Entrances[0].hasOwnProperty('entrance') &&
		spoiler.Entrances[0].hasOwnProperty('exit') &&
        spoiler.hasOwnProperty('meta')
}

const readJson = (file, onLoad) => {
	var reader = new FileReader()
	reader.onload = event => {
		let loadedJson
		try {
			loadedJson = JSON.parse(event.target.result)
		} catch(error) {
			console.error(error)
		}
		onLoad(loadedJson)
	}
	reader.readAsText(file)
}

class App extends Component {  
	constructor(props) {
		super(props)
		this.fileRef = React.createRef()
		this.state = this.baseState(JSON.parse(localStorage.getItem(ICONS)) || premarkedEntrances)
		listOfImages = this.importAll(require.context('./images/item_sprites/', false, /\.(png|jpe?g|svg)$/))
		listOfImages = { ...listOfImages, ...this.importAll(require.context('./images/bosses/', false, /\.(png|jpe?g|svg)$/))}
		listOfImages = { ...listOfImages, ...this.importAll(require.context('./images/connectors/', false, /\.(png|jpe?g|svg)$/))}
		listOfImages = { ...listOfImages, ...this.importAll(require.context('./images/misc/', false, /\.(png|jpe?g|svg)$/))}
		listOfItems = imageNames.reduce((obj, item) => {
			return {
				...obj,
				[item.image ? item.image : item.text]: item
			}; 
		}, {})
		const disabledItemKeys = Object.keys(listOfItems).filter(key => listOfItems[key].disabled === "true")
		disabledItemKeys.forEach(key => {if (!(key in this.state.disabledImages)) this.state.disabledImages[key] = true})
	}

  baseState = icons => {
  	return {
  		selectedIcon: null,
  		icons: icons,
  		iconPos: null,
  		iconSelectOpen: false,
  		resetConfirm: false,
  		lightWorld: true,
  		menuOpen: false,
  		isEdit: false,
  		disabledImages: JSON.parse(localStorage.getItem(DISABLED)) || {}
  	}
  }

  loadPremarkedEntrances = () => {
  	this.setState(this.baseState(premarkedEntrances))
  }

  loadPremarkedItems = () => {
  	this.setState(this.baseState(premarkedItems))
  }

  handleIconClick = e => {
  	if (e.evt.button === 2) {
  		this.handleIconDelete(e)
  		return
  	}
  	this.setState(prevState => ({
  		...prevState,
  		selectedIcon: prevState.icons.filter(icon => e.target.attrs.id === icon.id)[0],
  		icons: addToArrayIfExists(prevState.icons.filter(icon => e.target.attrs.id !== icon.id), prevState.selectedIcon),
  		iconSelectOpen: false,
  		isEdit: false,
  		iconPos: null,
  		menuOpen: false
  	}))
  	this.updateLocalStorage()
  }

  handleDirection = (e) => {
  	this.setState(prevState => (getStateForDirection(prevState, e.target.attrs.direction)))
  	this.updateLocalStorage()
  }

  handleSelectedIconDelete = e => {
  	this.setState(prevState => (getStateForSelectedDeleteIcon(prevState)))
  	this.updateLocalStorage()
  };

  handleIconDelete = e => {
  	this.setState(prevState => ({
  		...prevState,
  		selectedIcon: prevState.selectedIcon && prevState.selectedIcon.id === e.target.attrs.id ? null : prevState.selectedIcon,
  		icons: prevState.icons.filter(icon => e.target.attrs.id !== icon.id)
  	}))
  	this.updateLocalStorage()
  }

  handleIconSelect = (e) => {
  	this.setState(prevState => ({
  		...prevState,
  		selectedIcon: {
  			id: uuid.v1(),
  			x: prevState.iconPos.x,
  			y: prevState.iconPos.y,
  			src: e.target.attrs.srcName,
  			direction: null,
  			lightWorld: prevState.lightWorld},
  		iconPos: null,
  		iconSelectOpen: false,
  		isEdit: false
  	}))
  	this.updateLocalStorage()
  };

  handleBackgroundClick = e => {
  	if (this.state.iconSelectOpen) {
  		this.setState(prevState => ({
  			...prevState,
  			selectedIcon: null,
  			icons: addToArrayIfExists(prevState.icons, prevState.selectedIcon),
  			iconSelectOpen: false,
  			isEdit: false,
  			iconPos: null
  		}))
  	} else {
  		this.setState(prevState => ({
  			...prevState,
  			selectedIcon: null,
  			icons: addToArrayIfExists(prevState.icons, prevState.selectedIcon),
  			iconPos: {
  				x: e.target.getStage().getPointerPosition().x - ICON_SIZE / 2,
  				y: e.target.getStage().getPointerPosition().y - ICON_SIZE / 2},
  			iconSelectOpen: true,
  			isEdit: false,
  			menuOpen: false
  		}))
  	}
  	this.updateLocalStorage()
  }

  handleKeyDown = e => {
  	if (e.key === 'Escape') {
  		this.setState(prevState => ({
  			...prevState,
  			iconSelectOpen: false,
  			isEdit: false,
  			iconPos: null,
  			icons: addToArrayIfExists(prevState.icons, prevState.selectedIcon),
  			selectedIcon: null,
  			menuOpen: false
  		}))
  	}
  	if (e.key === 'ArrowUp') {
  		this.setState(prevState => (getStateForDirection(prevState, UP)))
  		e.preventDefault()
  	}
  	if (e.key === 'ArrowDown') {
  		this.setState(prevState => (getStateForDirection(prevState, DOWN)))
  		e.preventDefault()
  	}
  	if (e.key === 'ArrowLeft') {
  		this.setState(prevState => (getStateForDirection(prevState, LEFT)))
  		e.preventDefault()
  	}
  	if (e.key === 'ArrowRight') {
  		this.setState(prevState => (getStateForDirection(prevState, RIGHT)))
  		e.preventDefault()
  	}
  	if (e.key === 'Backspace' || e.key === 'Delete') {
  		this.setState(prevState => (getStateForSelectedDeleteIcon(prevState)))
  		e.preventDefault()
  	}
  	if (e.key === 'Tab') {
  		this.handleBackgroundSwitch()
  		e.preventDefault()
  	}
  	this.updateLocalStorage()
  }

  handleIconDrag = e => {
  	this.setState(prevState => ({
  		...prevState,
  		selectedIcon: moveIconIfMatches(prevState.selectedIcon, e),
  		icons: prevState.icons.map(icon => moveIconIfMatches(icon, e))
  	}))
  	this.updateLocalStorage()
  }

  handleResetPressed = () => {
  	this.setState(prevState => ({...prevState, resetConfirm:true}))
  }

  handleCancelReset = () => {
  	this.setState(prevState => ({...prevState, resetConfirm:false}))
  }

  handleReset = () => {
  	this.setState(this.baseState([]))
  	this.updateLocalStorage()
  }

  handleBackgroundSwitch = () => {
  	this.setState(prevState => ({
  		...prevState,
  		lightWorld: !prevState.lightWorld,
  		selectedIcon: null,
  		icons: addToArrayIfExists(prevState.icons, prevState.selectedIcon)
  	}))
  }

  handleMenu =() => {
  	this.setState(prevState => ({...prevState, menuOpen: prevState.iconSelectOpen ? false : !prevState.menuOpen}))
  }

  handleSave = () => {
  	const objectData = this.state.icons
  	const filename = 'cheamo_tracker.json'
  	const contentType = 'application/json;charset=utf-8;'
  	if (window.navigator && window.navigator.msSaveOrOpenBlob) {
  		var blob = new Blob([decodeURIComponent(encodeURI(JSON.stringify(objectData)))], { type: contentType })
  		navigator.msSaveOrOpenBlob(blob, filename)
  	} else {
  		var a = document.createElement('a')
  		a.download = filename
  		a.href = 'data:' + contentType + ',' + encodeURIComponent(JSON.stringify(objectData))
  		a.target = '_blank'
  		document.body.appendChild(a)
  		a.click()
  		document.body.removeChild(a)
  	}
  }

  handleLoadSelect = e => {
  	readJson(
  		e.target.files[0],
  		loaded => {
  			if (validateLoadedIcons(loaded)) {
  				this.setState(prevState => ({...prevState, icons: loaded, menuOpen: false}))
  			} else if (validateLoadedSpoiler(loaded)) {
				const icons = SpoilerParser.parseSpoiler(loaded)
				this.setState(prevState => ({...prevState, icons: icons, menuOpen: false}))
			}  
			else {
				const message = 'Issue with loaded file'
				window.alert(message)
  				console.log(message)
  			}
  		}
  	)
  }

  handleLoad = () => {
  	this.fileRef.current.click()
  }

  handleSelectToggle = () => {
  	this.setState(prevState => ({...prevState, isEdit: !prevState.isEdit}))
  }

  handleIconToggle = e => {
  	this.setState(prevState => {
  		prevState.disabledImages[e.target.attrs.srcName] = !(prevState.disabledImages[e.target.attrs.srcName] || false)
  		return prevState
	  })
	  this.updateLocalStorage()
  }

  importAll(r) {
  	let keyedImages = {}
  	r.keys().forEach(key => keyedImages[key] = r(key))
  	return keyedImages
  }

  updateLocalStorage(){
	  localStorage.setItem(ICONS, JSON.stringify(addToArrayIfExists(this.state.icons, this.state.selectedIcon)))
	  localStorage.setItem(DISABLED, JSON.stringify(this.state.disabledImages))
  }

  componentDidMount(){
  	window.addEventListener('keydown', this.handleKeyDown)
  }

  componentWillUnmount(){
  	window.removeEventListener('keydown', this.handleKeyDown)
  }

  render() {
  	return (
  		<div>
  			<Stage width={MAP_SIZE} height={MAP_SIZE} onContextMenu={(e) => e.evt.preventDefault()}>
  				<Background 
  					backgroundSrc={this.state.lightWorld ? lightWorld : darkWorld}
  					switchSrc={portal}
  					mapSize={MAP_SIZE}
  					menuSize={MENU_SIZE}
  					onBackgroundClick={this.handleBackgroundClick}
  					onBackgroundSwitch={this.handleBackgroundSwitch}
  				/>
  				<IconView 
  					icons={this.state.icons}  
  					iconSize={ICON_SIZE}
					listOfItems={listOfItems}
  					listOfImages={listOfImages}
  					iconPos={this.state.iconPos}
  					selectedIcon={this.state.selectedIcon}
  					onClick={this.handleIconClick}
  					onDrag={this.handleIconDrag}
  					onDirection={this.handleDirection}
  					lightWorld={this.state.lightWorld}
  					tempIconSrc={redX}
  				/>
  				<Menu 
  					rightLocation={MAP_SIZE}
  					isOpen={this.state.menuOpen}
  					onMenu={this.handleMenu}
  					onSave={this.handleSave}
  					onLoad={this.handleLoad}
  					onReset={this.handleResetPressed}
  				/>
  				{this.state.iconSelectOpen &&
					<Layer x={10} y={10}>
						<ItemSelector maxWidth={MAP_SIZE - 20}
							listOfItems={listOfItems}
							listOfImages={listOfImages}
							disabledImages={this.state.disabledImages}
							isEdit={this.state.isEdit}
							onSelectToggle={this.handleSelectToggle}
							onSelect={this.handleIconSelect}
							onIconToggle={this.handleIconToggle}/>
					</Layer>}
  				{this.state.resetConfirm &&
				  	<Reset
				  		mapSize={MAP_SIZE}
				  		onCancel={this.handleCancelReset}
				  		onConfirm={this.handleReset}
				  		onPremarkedEntrances={this.loadPremarkedEntrances}
				  		onPremarkedItems={this.loadPremarkedItems}
				  	/>
  				}
  			</Stage>
  			<input type="file" id="file" ref={this.fileRef} style={{display: 'none'}} onChange={this.handleLoadSelect}/>
  		</div>
  	)
  }
}

export default App