import uuid from 'uuid'
import entrancePositions from './entrances.json'
import exits from './exits.json'

class SpoilerParser {
    static parseSpoiler(spoiler) {
        var entrances = [];
        spoiler.Entrances.forEach(entrance => {
            if (entrance.entrance in entrancePositions) {
                if (entrance.exit in exits) {
                    entrances.push({
                        id: uuid.v1(),
                        x: entrancePositions[entrance.entrance].x,
                        y: entrancePositions[entrance.entrance].y,
                        src: "image" in exits[entrance.exit] ? exits[entrance.exit].image : exits[entrance.exit].text,
                        direction: exits[entrance.exit].direction,
                        lightWorld: entrancePositions[entrance.entrance].lightWorld
                    })
                }
            }
        });
        return entrances;
    }
}

export default SpoilerParser;