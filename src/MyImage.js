import React, { Component } from 'react'
import { Image } from 'react-konva'

class MyImage extends Component {
  state = {
  	image: null
  };

  loadImage() {
  	this.image = new window.Image()
  	this.image.src = this.props.src
  	this.image.addEventListener('load', this.handleLoad)
  }

  handleLoad = () => {
  	this.setState({
  		image: this.image
  	})
  };

  componentDidMount() {
  	this.loadImage()
  }

  componentDidUpdate(oldProps) {
  	if (oldProps.src !== this.props.src) {
  		this.loadImage()
  	}
  }

  componentWillUnmount() {
  	this.image.removeEventListener('load', this.handleLoad)
  }

  render() {
  	return <Image
  		{...this.props}
  		id={this.props.id}
  		image={this.state.image}
  		onClick={this.props.onClick}
  		onTap={this.props.onClick}
  		draggable={this.props.draggable}
  		x={this.props.x}
  		y={this.props.y}
  		width={this.props.width}
  		height={this.props.height}
  		onDragEnd={this.props.onDrag}
  		onDragMove={this.props.onDrag}
  		srcName={this.props.srcName}
  		shadowEnabled={true}
  		shadowOpacity={0.9}
  		shadowColor='black'
  		shadowOffsetX={3}
  		shadowOffsetY={3}
  	/>
  }
}

export default MyImage