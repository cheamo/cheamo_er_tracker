import React, { Component } from 'react'
import { Line, Group, Rect } from 'react-konva'

const ARROW_COLOR='RED'

class Arrow extends Component {
	render() {
		const {point1, point2, offset, ...childProps} = this.props

		const midPoint = (point1.x === point2.x) 
			? {x:point1.x + offset, y:(point1.y - point2.y) / 2  + point2.y}
			: {x:(point2.x - point1.x) / 2  + point1.x, y:point1.y + offset}
		return <Group>
			<Line
				{...this.props}
				points={[point1.x, point1.y, midPoint.x, midPoint.y, point2.x, point2.y]}
				stroke={ARROW_COLOR}
				strokeWidth={2}
			/>
			<Rect
				{...childProps}
				x={Math.min(point1.x, midPoint.x, point2.x)}
				y={Math.min(point1.y, midPoint.y, point2.y)}
				width={Math.max(point2.x - point1.x, Math.abs(offset))}
				height={Math.max(point2.y - point1.y, Math.abs(offset))}
				fill={'transparent'}
			/>
		</Group>
	}
}

export default Arrow