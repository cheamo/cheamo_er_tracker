import React, { Component } from 'react'
import { Layer } from 'react-konva'
import MyImage from './MyImage'
import Icon, { ALL } from './Icon'

class IconView extends Component {
	render() {
		const {iconSize, listOfItems, listOfImages, iconPos, selectedIcon, onClick, onDrag} = this.props
		return <Layer>
			{this.props.icons
				.filter(icon => this.props.lightWorld === icon.lightWorld)
				.map(icon =>
					<Icon
						key={icon.id}
						id={icon.id}
						src={listOfImages[icon.src]}
						text={icon.src in listOfItems ? listOfItems[icon.src].text : ""}
						x={icon.x}
						y={icon.y}
						width={iconSize}
						height={iconSize}
						draggable
						direction={icon.direction}
						handleIconClick={onClick}
						onDrag={onDrag}/>
				)}
			{(selectedIcon && selectedIcon !== null) &&
            <Icon
            	id={selectedIcon.id}
            	src={listOfImages[selectedIcon.src]}
				text={selectedIcon.src in listOfItems ? listOfItems[selectedIcon.src].text : ""}
            	x={selectedIcon.x}
            	y={selectedIcon.y}
            	width={iconSize}
            	height={iconSize}
            	draggable
            	direction={ALL}
            	onClickDelete={this.handleSelectedIconDelete}
            	handleIconClick={onClick}
            	handleDirection={this.props.onDirection}
            	onDrag={onDrag}/>
			}
			{iconPos &&
            <MyImage
            	src={this.props.tempIconSrc}
            	x={iconPos.x}
            	y={iconPos.y}
            	width={iconSize}
            	height={iconSize}/>
			}
		</Layer>
	}
}

export default IconView