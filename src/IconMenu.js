import React, {Component} from 'react'
import {Text, Group} from 'react-konva'

class IconMenu extends Component {
	render() {
		const iconSize = this.props.iconSize
		return <Group>
			{/* placeholders for real controls */}
			<Text text='<' x={-iconSize / 2} y={iconSize / 2} onClick={this.props.onDirection}/>
			<Text text='<' x={iconSize / 2} y={-iconSize / 2} rotation={90}  onClick={this.props.onDirection}/>
			<Text text='>' x={iconSize / 2 * 3} y={iconSize / 2} onClick={this.props.onDirection}/>
			<Text text='>' x={iconSize / 2} y={iconSize / 2 * 3} rotation={90} onClick={this.props.onDirection}/>
			<Text text='D' x={iconSize / 2 * 3} y={iconSize / 2 * 3} onClickDelete={this.props.onClickDelete}/>
		</Group>
	}
}

export default IconMenu