import React, { Component } from 'react'
import { Group, Text } from 'react-konva'
import MyImage from './MyImage'
import Arrow from './Arrow'

export const LEFT = 'LEFT'
export const UP = 'UP'
export const RIGHT = 'RIGHT'
export const DOWN = 'DOWN'
export const ALL = 'ALL'

const ARROW_COLOR='black'
const ARROW_DEPTH=4

class Icon extends Component {
	componentDidMount() {
		this.textWidth = this.text1 ? this.text1.textWidth  : 50 
		this.textHeight = this.text1 ? this.text1.textHeight : 50
		this.setState({});
	}
	
	render() {
		const iconWidth = this.props.width
		const iconHeight = this.props.height
		return <Group>
			{this.props.src ? <MyImage
				id={this.props.id}
				src={this.props.src}
				srcName={this.props.srcName}
				x={this.props.x}
				y={this.props.y}
				width={iconWidth}
				height={iconHeight}
				draggable={this.props.draggable}
				onClick={this.props.handleIconClick}
				onDrag={this.props.onDrag} 
				opacity={this.props.opacity}/>
			: <Text
				id={this.props.id}
				text={this.props.text}
				srcName={this.props.srcName}
				stroke='white'
				fill='black'
				strokeWidth={3}
				fillAfterStrokeEnabled='true'
				fontFamily='verdana'
				fontSize={20}
				fontStyle='bold'
				scaleX={iconWidth / this.textWidth}
				scaleY={iconHeight / this.textHeight}
				align='center'
				verticalAlign='middle'
				x={this.props.x}
				y={this.props.y}
				draggable={this.props.draggable}
				onClick={this.props.handleIconClick}
				onDragEnd={this.props.onDrag}
  				onDragMove={this.props.onDrag}
				opacity={this.props.opacity}
				ref={node => {
					this.text1 = node;
				  }}
				/>}
			<Group x={this.props.x} y={this.props.y}>
				{(this.props.direction === ALL || this.props.direction === LEFT) &&
                    <Arrow
                    	id={this.props.id}
                    	text='<'
                    	sides={3}
                    	width={ARROW_DEPTH}
                    	height={iconHeight}
                    	point1={{x:0, y:0 + ARROW_DEPTH}}
                    	point2={{x:0, y:iconHeight - ARROW_DEPTH}}
                    	offset={-ARROW_DEPTH}
                    	fill={ARROW_COLOR}
                    	direction={LEFT}
                    	onClick={this.props.handleDirection}
                    	onTap={this.props.handleDirection} />}
				{(this.props.direction === ALL || this.props.direction === UP) &&
                    <Arrow
                    	id={this.props.id}
                    	text='<'
                    	width={iconWidth}
                    	height={ARROW_DEPTH}
                    	point1={{x:0 + ARROW_DEPTH, y:0}}
                    	point2={{x:iconWidth - ARROW_DEPTH, y:0}}
                    	offset={-ARROW_DEPTH}
                    	fill={ARROW_COLOR}
                    	direction={UP}
                    	onClick={this.props.handleDirection}
                    	onTap={this.props.handleDirection} />}
				{(this.props.direction === ALL || this.props.direction === RIGHT) &&
                    <Arrow
                    	id={this.props.id}
                    	text='>'
                    	width={ARROW_DEPTH}
                    	height={iconHeight}
                    	point1={{x:iconWidth, y:0 + ARROW_DEPTH}}
                    	point2={{x:iconWidth, y:iconHeight - ARROW_DEPTH}}
                    	offset={ARROW_DEPTH}
                    	fill={ARROW_COLOR}
                    	direction={RIGHT}
                    	onClick={this.props.handleDirection}
                    	onTap={this.props.handleDirection} />}
				{(this.props.direction === ALL || this.props.direction === DOWN) &&
                    <Arrow
                    	id={this.props.id}
                    	text='>'
                    	width={iconWidth}
                    	height={ARROW_DEPTH}
                    	point1={{x:0 + ARROW_DEPTH, y:iconHeight}}
                    	point2={{x:iconWidth - ARROW_DEPTH, y:iconHeight}}
                    	offset={ARROW_DEPTH}
                    	fill={ARROW_COLOR}
                    	direction={DOWN}
                    	onClick={this.props.handleDirection} 
                    	onTap={this.props.handleDirection}/>}
			</Group>
		</Group>
	}
}

export default Icon